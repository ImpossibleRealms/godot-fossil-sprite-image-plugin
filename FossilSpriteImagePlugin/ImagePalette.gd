#class ImagePalette : public Object{
#	GDCLASS(ImagePalette, Object);
#	
#	ClassDB::register_class<MyCustomClass>()
#}
extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var imageTexture
var dynImage
var mat

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _init(material):
	mat = material
	imageTexture = ImageTexture.new()
	dynImage = Image.new()
	dynImage.copy_from(material.get_shader_param("palette").get_data())

func alterPalette(colorIDs, colors):
	#dynImage.create(imageWidth, imageHeight, false, Image.FORMAT_RGBA8)
	dynImage.lock()
	for i in range(0, colorIDs.size()):
		dynImage.set_pixel(colorIDs[i]%256, colorIDs[i]/256, colors[i])
	#dynImage.set_pixel(0, 0, Color(sin(timeThingy), sin(timeThingy), sin(timeThingy), 1))
	#dynImage.set_pixel(1, 0, Color(cos(timeThingy), cos(timeThingy), cos(timeThingy), 1))
	dynImage.unlock()
	
	var imageFlags = 0
	imageTexture.create_from_image(dynImage, imageFlags)
	imageTexture.set_lossy_storage_quality(ImageTexture.STORAGE_COMPRESS_LOSSLESS)
	mat.set_shader_param("palette", imageTexture)
	pass
	
func switchPalette(texture):
	mat.set_shader_param("palette", texture)
	pass