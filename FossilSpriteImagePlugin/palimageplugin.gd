tool
extends EditorPlugin

var import_plugin
#var irimage

func _enter_tree():
	add_custom_type("IRImageTexture", "ImageTexture", preload("ir_image.gd"), preload("icon.png"))
	import_plugin = preload("import_plugin.gd").new()
	add_import_plugin(import_plugin)

func _exit_tree():
	remove_import_plugin(import_plugin)
	remove_custom_type("IRImageTexture")
	import_plugin = null