# import_plugin.gd
tool
extends EditorImportPlugin

enum Presets { PRESET_DEFAULT }

func get_preset_count():
    return Presets.size()
    
func get_preset_name(preset):
    match preset:
        Presets.PRESET_DEFAULT:
            return "Default"
        _:
            return "Unknown"

#warning-ignore:unused_argument
#warning-ignore:unused_argument
func get_option_visibility(option, options):
    return true

func get_import_options(preset):
	match preset:
		Presets.PRESET_DEFAULT:
            return [{
                       "name": "filter",
                       "default_value": false
                    }]
			#return []
		_:
			return []

func get_importer_name():
    return "importers.ir_paletted"

func get_visible_name():
    return "IR Paletted Image"

func get_recognized_extensions():
    return ["obg", "tbg", "fbg", "ebg", "sbg", "spal"]

func get_save_extension():
    return "res"

func get_resource_type():
    return "ImageTexture"

#warning-ignore:unused_argument
#warning-ignore:unused_argument
#warning-ignore:unused_argument
func import(source_file, save_path, options, r_platform_variants, r_gen_files):
	var colors = []
	var usesInlinePalette = false
	var isPalette = false

	var file = File.new()
	var err = file.open(source_file, File.READ)
	if err != OK:
		return err

	#var line = file.get_line()


	#First, load the file information
	if(source_file.get_extension() == "spal"):
		isPalette=true
	
	var fileRev
	var temp = 0
	var imageWidth
	var imageHeight
	var imageData = []

	var numColors
#warning-ignore:unused_variable
	var color1Transparency = false
#warning-ignore:unused_variable
	var color2Transparency = false
	var twoByteWidth = false
	var twoByteHeight = false

	if(!isPalette):
		fileRev = file.get_8()
		if(fileRev == 0):
			temp = file.get_8()
			if(temp >> 7 == 1):
				usesInlinePalette=true
				if((temp >> 6 & 1) == 1):
					color1Transparency=true
				if((temp >> 5 & 1) == 1):
					color2Transparency=true
			if((temp >> 4 & 1) == 1):
				twoByteWidth=true
			if((temp >> 3 & 1) == 1):
				twoByteHeight=true
		else:
			return ERR_PARSE_ERROR
	#TODO: Add support for revision 1
	elif(isPalette):
		numColors = file.get_8() << 8
		#numColors = file.get_8()
		#print("Number of colors in " + source_file + ": " + numColors as String)
		#numColors = numColors << 8
		#print("Number of colors in " + source_file + ": " + numColors as String)
		numColors += file.get_8()
		
		print("Number of colors in " + source_file + ": " + numColors as String)
		
		imageWidth = 256
		
		imageHeight = int((numColors/256)+1)
		print("imageHeight: " + imageHeight as String)
		#imageHeight = 1
		
		for i in range(0, numColors):
			colors.append(Color((file.get_8())/255.0, (file.get_8())/255.0, (file.get_8())/255.0, (file.get_8())/255.0))
			print(colors[i])
		#for i in range(0, imageHeight):
		#	imageData.append([])
		#for j in range(0, imageHeight):
		#	for i in range(0, 256):
		#		#imageData[i].append([])
		#		if(i < numColors%256):
		#			imageData[j].append(Color((file.get_8())/255.0, (file.get_8())/255.0, (file.get_8())/255.0, (file.get_8())/255.0))
		#		else:
		#			imageData[j].append(Color(0.0, 0.0, 0.0, 0.0))
	
		#TODO: Add inline palette support
	
	if(!isPalette):
		if(usesInlinePalette):
			usesInlinePalette=true
		else:
			var palFile = File.new()
			err = palFile.open(source_file.get_basename()+".spal", file.READ)
			if err != OK:
				return err

			numColors = palFile.get_8() << 8
			numColors += palFile.get_8()

        #temp = 0

#warning-ignore:unused_variable
			for i in range(0, numColors):
				colors.append(Color((palFile.get_8())/255.0, (palFile.get_8())/255.0, (palFile.get_8())/255.0, (palFile.get_8())/255.0))

			palFile.close()

		if(fileRev == 0):
		# Now use the gathered file header information to get the image's size
			if(twoByteWidth):
				imageWidth = file.get_8() << 8
				imageWidth = imageWidth + file.get_8()
			else:
				imageWidth = file.get_8()
				if(imageWidth==0):
					imageWidth = 256

			if(twoByteHeight):
				imageHeight = file.get_8() << 8
				imageHeight = imageHeight + file.get_8()
			else:
				imageHeight = file.get_8()
				if(imageHeight==0):
					imageHeight = 256
		
		print("Width of " + source_file + ": " + imageWidth as String)

		var tempPixel
		var offset = 0

		var numBits = 1
		
		if(source_file.get_extension() == "tbg"):
			numBits = 2
		elif(source_file.get_extension() == "fbg"):
			numBits = 4
		elif(source_file.get_extension() == "ebg"):
			numBits = 8
		elif(source_file.get_extension() == "sbg"):
			numBits = 16

		# Finally, use the image's size to gather the image data
#warning-ignore:unused_variable
		for i in range(0, imageWidth):
			imageData.append([])

		tempPixel = 0
		offset = 0

		for j in range(0, imageHeight):
			for i in range(0, imageWidth):
				if(offset%8 == 0):
					temp = file.get_8()
				tempPixel = 0
#warning-ignore:unused_variable
				for k in range (0, numBits):
					if((offset%8) == 0 && k > 0):
						temp = file.get_8()
					tempPixel = tempPixel << 1
					tempPixel = tempPixel + ((temp >> 7-(offset%8)) & 1)
					offset+=1
				imageData[i].append([])
				imageData[i][j] = tempPixel

	file.close()

	#Now to use the loaded data to create a texture!
	var imageTexture = ImageTexture.new()
	var dynImage = Image.new()

	dynImage.create(imageWidth, imageHeight, false, Image.FORMAT_RGBA8)
	dynImage.lock()
	for j in range(0, imageHeight):
		for i in range(0, imageWidth):
				#if(i==3 && j==2):
				#	print(imageData[i][j])
			if(isPalette):
				if((j*imageWidth)+i >= numColors):
					dynImage.set_pixel(i, j, Color(0.0, 0.0, 0.0, 0.0))
				else:
					dynImage.set_pixel(i, j, colors[(j*imageWidth)+i])
			else:
				#dynImage.set_pixel(i, j, colors[imageData[i][j]])
				dynImage.set_pixel(i, j, Color((imageData[i][j])/255.0, (imageData[i][j] >> 8)/255.0, 0.0, 0.0))
				#dynImage.set_pixel(i, j, Color(1,1,1))
	dynImage.unlock()
	
	var imageFlags = 0
	
	if options.filter:
		imageFlags = ImageTexture.FLAG_FILTER
		print("Filter is on")
	else:
		print("filter is off")
	
	imageTexture.create_from_image(dynImage, imageFlags)
	imageTexture.set_lossy_storage_quality(ImageTexture.STORAGE_COMPRESS_LOSSLESS)
	
	#var imageTexture2 = ImageTexture.new()
	#imageTexture2.create(64, 64, Image.FORMAT_RGBA8, ImageTexture.FLAG_ANISOTROPIC_FILTER)
	#self.texture = imageTexture
	
	#imageTexture.resource_name = "The created texture!"
	#print(imageWidth)
	#print(self.texture.resource_name)
	#return ERR_PARSE_ERROR
	return ResourceSaver.save("%s.%s" % [save_path, get_save_extension()], imageTexture)

	#file.close()
